import numpy as np
import csv
import pandas as pd



def loadcsv(q):
  data=pd.read_csv(q,header=1,sep=" ")
  # print(data)
  # print(dir(data))
  # print(type(data))
  # print(list(data["image_name"]))
  
  ret={}
  for key in data.keys():
    ret[key]=list(data[key])
  return ret
  
def invdic(q):
  return [{k:q[k][i] for k in q.keys()} for i in range(len(q[list(q.keys())[0]]))]
def subdic(q,key="image_name"):
  ret={}
  for p in q:
    ret[p[key]]=p
  return ret

def mergedic(*q):
  ret={}
  for qq in q:
    for key in qq.keys():
      ret[key]=qq[key]
  return ret

def mergedicts(*q):
  ret={}
  for key in q[0].keys():
    if not np.sum([1 for qq in q if key in qq.keys()])==len(q):continue
    
    ret[key]=mergedic(*[qq[key] for qq in q])
  return ret

def testnan(q):
  try:
    if type(q) is str:return q=="nan"
    return np.isnan(q)
  except:
    print("could not nan ana at")
    print(q)
    print(type(q))
    exit()
  
  exit()

def nonans(q):
  ret={}
  for key in q.keys():
    if np.sum([1 for qq in q[key].keys() if testnan(q[key][qq])])>0:continue
    ret[key]=q[key]
  
  return ret

def load():
  bbox=subdic(invdic(loadcsv("data/list_bbox.txt")))
  joints=subdic(invdic(loadcsv("data/list_joints.txt")))
  landmarks=subdic(invdic(loadcsv("data/list_landmarks.txt")))
  
  d=mergedicts(bbox,joints,landmarks)
  
  return d
  print(d[list(d.keys())[0]])

def getbbox():
  return subdic(invdic(loadcsv("data/list_bbox.txt")))
def getjoints():
  return subdic(invdic(loadcsv("data/list_joints.txt")))
def getlandmarks():
  return subdic(invdic(loadcsv("data/list_landmarks.txt")))


  
if __name__=="__main__":
  d=load()
  d2=nonans(d)
  
  print(len(d.keys()))
  print(len(d2.keys()))









