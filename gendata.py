import numpy as np

from load import *

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np

img="img_00000114"


plot=False

def trafo(px,py,x1,x2,y1,y2):
  dx=np.abs(x2-x1)
  dy=np.abs(y2-y1)
  x=[(ax-x1)/dx for ax in px]
  y=[1-(ay-y1)/dy for ay in py]
  return x,y


bbox=getbbox()
landmark=getlandmarks()



def doimg(img):

  if plot:im = np.array(Image.open(f'imgs/{img}.jpg'), dtype=np.uint8)

  # Create figure and axes
  if plot:fig,ax = plt.subplots(1)

  # Display the image
  if plot:ax.imshow(im)






  # x=load()


  # print(x[list(x.keys())[0]])
  y=bbox[f"img/{img}.jpg"]

  x1=y["x_1"]
  x2=y["x_2"]
  y1=y["y_1"]
  y2=y["y_2"]


  # Create a Rectangle patch
  if plot:rect = patches.Rectangle((x1,y1),x2-x1,y2-y1,linewidth=1,edgecolor='r',facecolor='none')

  # Add the patch to the Axes
  if plot:ax.add_patch(rect)

  y=landmark[f"img/{img}.jpg"]

  typ=y["clothes_type"]

  #typ=3 for full body, 1 for upper body (hat dann nur 6 landmarks)

  if not int(typ)==3:return False,None

  vtyp=y["variation_type"]
  #1 2 3 should be ok
  
  if int(vtyp)>3:return False,None
  

  px,py=[],[]
  for i in range(1,9):
    px.append(y[f"landmark_location_x_{i}"])
    py.append(y[f"landmark_location_y_{i}"])
  if plot:plt.plot(px,py,"o",color="red")
  
  if plot:plt.show()
  
  if "nan" in "".join([str(ax) for ax in px]):return False,None
  if "nan" in "".join([str(ay) for ay in py]):return False,None

  return True,trafo(px,py,x1,x2,y1,y2)



  # y=getjoints()[f"img/{img}.jpg"]
  # px,py=[],[]
  # for i in range(1,15):
    # px.append(y[f"joint_location_x_{i}"])
    # py.append(y[f"joint_location_y_{i}"])
    
  # print(px,py)
    
  # plt.plot(px,py,"o",color="blue")

xx,yy=[],[]
imgs=[]


for img in bbox.keys():
  img=img[4:img.find(".")]
  val,ac=doimg(img)
  if not val:continue
  x,y=ac
  xx.append(x)
  yy.append(y)
  imgs.append(img)
  print(f"did {img}")



d=np.concatenate((xx,yy),axis=1)
d=np.reshape(d,(d.shape[0],2,8))
d=np.transpose(d,(0,2,1))



print("saving")
np.savez_compressed("data",x=xx,y=yy,i=imgs,d=d)
print("done")

exit()


val,(x,y)=(doimg(img))

plt.plot(x,y,"o")


if not plot:plt.show()




